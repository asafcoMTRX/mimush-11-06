﻿(function () {

    var app = angular.module('metropolinetApp', ['ui.router', 'ui.bootstrap', 'ngFileUpload', 'ngSanitize']);

    app.config(['$locationProvider', '$stateProvider', '$urlRouterProvider',
        function ($locationProvider, $stateProvider, $urlRouterProvider) {

            var viewBase = '/app/metropolinetApp/views/';
            var partialViewBase = '/app/metropolinetApp/partials/';

            $urlRouterProvider.otherwise("/home");

            $stateProvider
                .state('home',
                {
                    url: '/home?municipalityId',
                    params: { municipalityId: '70' },
                    views: {
                        '': {
                            templateUrl: viewBase + 'home.html',
                            controller: "HomeController",
                            controllerAs: 'vm',
                            resolve: {
                                municipalityDetails: ['$stateParams', 'municipalityService', function ($stateParams, municipalityService) {
                                    return municipalityService.getMunicipalityIntroViewModel($stateParams.municipalityId);
                                }]
                            }
                        },
                        'municipalityIntro@home': {
                            templateUrl: partialViewBase + 'municipalityIntro.html'
                        },
                        'loginForm@home': {
                            templateUrl: partialViewBase + 'loginForm.html'
                        }
                    }
                })
                .state('registration',
                {
                    url: "/registration",
                    views: {
                        '': {
                            templateUrl: viewBase + 'registration.html',
                            controller: 'RegistrationController',
                            controllerAs: 'vm',
                            data: { requireLogin: true }
                        },
                        'registrationForm@registration': {
                            templateUrl: partialViewBase + 'registrationForm.html'
                        }
                    }
                });

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        }]);

}());